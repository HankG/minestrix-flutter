import 'package:flutter/material.dart';

class MinesTrixTheme {
  static const LinearGradient buttonGradient = LinearGradient(
    colors: <Color>[
      Color(0xFFd24800),
      Color(0xFF0000a4),
    ],
  );
}
